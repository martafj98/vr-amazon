﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Take : MonoBehaviour
{
    public Renderer myRenderer;
    public Rigidbody myRigidbody;

    private void Awake()
    {
        myRenderer = GetComponent<Renderer>();
        myRigidbody = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Color l_color = myRenderer.material.color;
        l_color = Color.yellow;
        myRenderer.material.color = l_color;
    }

    private void OnTriggerExit(Collider other)
    {
        Color l_color = myRenderer.material.color;
        l_color = Color.white;
        myRenderer.material.color = l_color;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
