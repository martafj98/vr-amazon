﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject estanteria;
    public Button botonOtraEstanteria;
    public string accion = "Entra";
    public bool estanteriaActiva = false;

    public KeyCode buttonPress;

    float lastTime = 0;
    float cadency = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(buttonPress))
        {
            OnButtonPress();    
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Dedito")
        {
            OnButtonPress();
        }
    }
    public void OnButtonPress()
    {
        if (Time.realtimeSinceStartup - lastTime < cadency)
            return;

        lastTime = Time.realtimeSinceStartup;

        GetComponent<Animator>().Play("Press");
        
        if (estanteriaActiva)
        {
            Sale();
        }
        else
        {
            Entra();

            if (botonOtraEstanteria.estanteriaActiva == true)
            {
                botonOtraEstanteria.Sale();
            }
        }
    }

    public void Entra()
    {
        estanteria.SetActive(true);
        estanteriaActiva = true;
        estanteria.GetComponent<Animator>().SetTrigger("Entra");
        estanteria.GetComponent<AudioSource>().Play();
    }

    public void Sale()
    {
        estanteria.GetComponent<Animator>().SetTrigger("Sale");
        estanteriaActiva = false;
    }
}
