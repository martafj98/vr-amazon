﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encender : MonoBehaviour
{
    ParticleSystem corazon;
    // Start is called before the first frame update
    void Start()
    {
        corazon = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            corazon.Emit(1);
            foreach(ParticleSystem systemP in corazon.GetComponentsInChildren<ParticleSystem>())
            {
                systemP.Emit(1);
            }
        }
    }
}
