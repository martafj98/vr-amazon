﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;

public class Hand : MonoBehaviour
{
    public OVRInput.Button grabButton;
    Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(grabButton))
        {
            anim.SetBool("Take", true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //Coger
        if (OVRInput.GetDown(grabButton))
        {
            Take l_take = other.GetComponent<Take>();
            if (l_take)
            {
                l_take.myRigidbody.isKinematic = true;
                l_take.transform.parent = this.transform;
            }
        }

        //Soltar
        if (OVRInput.GetUp(grabButton))
        {
            Take l_take = other.GetComponent<Take>();

            if (l_take)
            {
                l_take.myRigidbody.isKinematic = false;
                l_take.transform.parent = null;
            }
        }
    }
}
